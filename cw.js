var common = ['the','of','to','and','a','in','is','it','you','that','he','was','for','on','are','with','as','I','his','they','be','at','one',
'have','this','from','or','had','by','hot','word','but','what','some','we','can','out','other','were','all','there','when','up','use','your','how','said','an','each',
'she','which','do','their','time','if','will','way','about','many','then','them','write','would','like','so','these','her','long','make','thing','see','him','two','has',
'look','more','day','could','go','come','did','number','sound','no','most','people','my','over','know','water','than','call','first','who','may','down','side','been',
'now','find','any','new','work','part','take','get','place','made','live','where','after','back','little','only','round','man','year','came','show','every','good','me',
'give','our','under','name','very','through','just','form','sentence','great','think','say','help','low','line','differ','turn','cause','much','mean','before','move','right',
'boy','old','too','same','tell','does','set','three','want','air','well','also','play','small','end','put','home','read','hand','port','large','spell','add','even','land','here',
'must','big','high','such','follow','act','why','ask','men','change','went','light','kind','off','need','house','picture','try','us','again','animal','point','mother','world',
'near','build','self','earth','father','head','stand','own','page','should','country','found','answer','school','grow','study','still','learn','plant','cover','food','sun','four',
'between','state','keep','eye','never','last','let','thought','city','tree','cross','farm','hard','start','might','story','saw','far','sea','draw','left','late','run','don\'t','while',
'press','close','night','real','life','few','north','open','seem','together','next','white','children','begin','got','walk','example','ease','paper','group','always','music','those',
'both','mark','often','letter','until','mile','river','car','feet','care','second','book','carry','took','science','eat','room','friend','began','idea','fish','mountain','stop','once',
'base','hear','horse','cut','sure','watch','color','face','wood','main','enough','plain','girl','usual','young','ready','above','ever','red','list','though','feel','talk','bird','soon',
'body','dog','family','direct','pose','leave','song','measure','door','product','black','short','numeral','class','wind','question','happen','complete','ship','area','half','rock','order',
'fire','south','problem','piece','told','knew','pass','since','top','whole','king','space','heard','best','hour','better','true','during','hundred','five','remember','step','early','hold',
'west','ground','interest','reach','fast','verb','sing','listen','six','table','travel','less','morning','ten','simple','several','vowel','toward','war','lay','against','pattern',
'slow','center','love','person','money','serve','appear','road','map','rain','rule','govern','pull','cold','notice','voice','unit','power','town','fine','certain','fly','fall','lead',
'cry','dark','machine','note','wait','plan','figure','star','box','noun','field','rest','correct','able','pound','done','beauty','drive','stood','contain','front','teach','week','final',
'gave','green','oh','quick','develop','ocean','warm','free','minute','strong','special','mind','behind','clear','tail','produce','fact','street','inch','multiply','nothing','course',
'stay','wheel','full','force','blue','object','decide','surface','deep','moon','island','foot','system','busy','test','record','boat','common','gold','possible','plane','stead','dry',
'wonder','laugh','thousand','ago','ran','check','game','shape','equate','hot','miss','brought','heat','snow','tire','bring','yes','distant','fill','east','paint','language','among','grand',
'ball','yet','wave','drop','heart','am','present','heavy','dance','engine','position','arm','wide','sail','material','size','vary','settle','speak','weight','general','ice','matter',
'circle','pair','include','divide','syllable','felt','perhaps','pick','sudden','count','square','reason','length','represent','art','subject','region','energy','hunt','probable','bed',
'brother','egg','ride','cell','believe','fraction','forest','sit','race','window','store','summer','train','sleep','prove','lone','leg','exercise','wall','catch','mount','wish','sky',
'board','joy','winter','sat','written','wild','instrument','kept','glass','grass','cow','job','edge','sign','visit','past','soft','fun','bright','gas','weather','month','million','bear',
'finish','happy','hope','flower','clothe','strange','gone','jump','baby','eight','village','meet','root','buy','raise','solve','metal','whether','push','seven','paragraph','third','shall',
'held','hair','describe','cook','floor','either','result','burn','hill','safe','cat','century','consider','type','law','bit','coast','copy','phrase','silent','tall','sand','soil','roll',
'temperature','finger','industry','value','fight','lie','beat','excite','natural','view','sense','ear','else','quite','broke','case','middle','kill','son','lake','moment','scale','loud',
'spring','observe','child','straight','consonant','nation','dictionary','milk','speed','method','organ','pay','age','section','dress','cloud','surprise','quiet','stone','tiny','climb','cool',
'design','poor','lot','experiment','bottom','key','iron','single','stick','flat','twenty','skin','smile','crease','hole','trade','melody','trip','office','receive','row','mouth','exact',
'symbol','die','least','trouble','shout','except','wrote','seed','tone','join','suggest','clean','break','lady','yard','rise','bad','blow','oil','blood','touch','grew','cent','mix','team',
'wire','cost','lost','brown','wear','garden','equal','sent','choose','fell','fit','flow','fair','bank','collect','save','control','decimal','gentle','woman','captain','practice','separate',
'difficult','doctor','please','protect','noon','whose','locate','ring','character','insect','caught','period','indicate','radio','spoke','atom','human','history','effect','electric','expect',
'crop','modern','element','hit','student','corner','party','supply','bone','rail','imagine','provide','agree','thus','capital','won\'t','chair','danger','fruit','rich','thick','soldier',
'process','operate','guess','necessary','sharp','wing','create','neighbor','wash','bat','rather','crowd','corn','compare','poem','string','bell','depend','meat','rub','tube','famous',
'dollar','stream','fear','sight','thin','triangle','planet','hurry','chief','colony','clock','mine','tie','enter','major','fresh','search','send','yellow','gun','allow','print','dead',
'spot','desert','suit','current','lift','rose','continue','block','chart','hat','sell','success','company','subtract','event','particular','deal','swim','term','opposite','wife','shoe',
'shoulder','spread','arrange','camp','invent','cotton','born','determine','quart','nine','truck','noise','level','chance','gather','shop','stretch','throw','shine','property','column',
'molecule','select','wrong','gray','repeat','require','broad','prepare','salt','nose','plural','anger','claim','continent','oxygen','sugar','death','pretty','skill','women','season',
'solution','magnet','silver','thank','branch','match','suffix','especially','fig','afraid','huge','sister','steel','discuss','forward','similar','guide','experience','score','apple','bought',
'led','pitch','coat','mass','card','band','rope','slip','win','dream','evening','condition','feed','tool','total','basic','smell','valley','nor','double','seat','arrive','master','track',
'parent','shore','division','sheet','substance','favor','connect','post','spend','chord','fat','glad','original','share','station','dad','bread','charge','proper','bar','offer','segment',
'slave','duck','instant','market','degree','populate','chick','dear','enemy','reply','drink','occur','support','speech','nature','range','steam','motion','path','liquid','log','meant',
'quotient','teeth','shell','neck'];

var commonQso = ['AND', 'THE', 'YOU', 'THAT', 'A', 'TO', 'KNOW', 'OF', 'IT', 'YEAH', 'IN', 'THEY', 'DO', 'SO', 'BUT', 'IS', 'LIKE', 'HAVE', 'WAS', 'WE', 'ITS', 'JUST', 'ON', 'OR', 'NOT', 'THINK', 'FOR', 'WELL', 'WHAT', 'ABOUT', 'ALL', 'THATS', 'OH', 'REALLY', 'ONE', 'ARE', 'RIGHT', 'UH', 'THEM', 'AT', 'THERE', 'MY', 'MEAN', 'DONT', 'NO', 'WITH', 'IF', 'WHEN', 'CAN', 'AS', 'HIS', 'FROM', 'HAD', 'BY', 'SOME', 'WERE', 'OUT', 'OTHER', 'WHERE', 'YOUR', 'UP', 'QRL', 'QRM', 'QRN', 'QRQ', 'QRS', 'QRZ', 'QTH', 'QSB', 'QRP', 'QSY', 'R', 'TU', 'RTU', 'TNX', 'NAME', 'RST', 'CQ', 'AGN', 'ANT', 'INV', 'V', 'DIPOLE', 'BEAM', 'ENDFED', 'G5RV', 'WINDOM', 'OCF', 'SLOPER', 'VERTICAL', 'DX', 'ES', 'FB', 'GM', 'GA', 'GE', 'HI', 'HR', 'HW', 'NR', 'OM', 'PSE', 'PWR', 'WX', '73', 'BK', 'KN', 'RIG', 'QRP', 'AGE', 'YRS', 'TEST', 'ICOM', 'KNWD', 'YAESU', 'TENTEC', 'ELECRAFT', 'HEATHKIT', 'HOMEBREW', 'MFJ', 'PSK', 'SWR', 'WX', 'TEMP', 'WARM', 'HOT', 'COLD', 'SUNNY', 'COOL', 'CLOUDY', 'CLDY', 'CLEAR', 'CLR', 'RAIN', 'SNOW', 'SLEET', 'HAIL', 'WINDY', 'WNDY', 'FOG', 'HOUR', 'HW?', 'CW', 'AM', 'SSB', 'USB', 'LSB', 'PSK31', 'FT8', 'PSK', 'DIGITAL', 'BTU', 'HAM', 'BEEN', 'RADIO', 'CALL', 'OP'];
var alphanumeric = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0"];
var Encoder = function () {
    var self = this;
    self.mapping = {
        "A": ".-",
        "B": "-...",
        "C": "-.-.",
        "D": "-..",
        "E": ".",
        "F": "..-.",
        "G": "--.",
        "H": "....",
        "I": "..",
        "J": ".---",
        "K": "-.-",
        "L": ".-..",
        "M": "--",
        "N": "-.",
        "O": "---",
        "P": ".--.",
        "Q": "--.-",
        "R": ".-.",
        "S": "...",
        "T": "-",
        "V": "...-",
        "U": "..-",
        "W": ".--",
        "X": "-..-",
        "Y": "-.--",
        "Z": "--..",
        "1": ".----",
        "2": "..---",
        "3": "...--",
        "4": "....-",
        "5": ".....",
        "6": "-....",
        "7": "--...",
        "8": "---..",
        "9": "----.",
        "0": "-----",
        ".": ".-.-.-",
        "?": "..--..",
        ",": "--..--",
        "=": "-...-",
        "<SK>": "...-.-",
        "/": "-..-.",
        " ": " ",
    }
}

Encoder.prototype.encode = function (word) {
    if (word.indexOf("<") !== -1) {
        return word.toUpperCase() in this.mapping ? this.mapping[word.toUpperCase()] : "";
    }
    return word.split("").map(x => x.toUpperCase() in this.mapping ? this.mapping[x.toUpperCase()] : "").join(" ");
}
var Sounder = function () {
    var self = this;

    this.rampTime = 0.003;
    this.context = new AudioContext();
    this.oscillator = this.context.createOscillator();
    this.gainNode = this.context.createGain();
    this.gainNode.gain.setValueAtTime(0, this.context.currentTime);
    this.oscillator.frequency.setValueAtTime(600, this.context.currentTime);
    this.oscillator.connect(this.gainNode);
    this.gainNode.connect(this.context.destination);
    this.oscillator.start(0);
}

Sounder.prototype.setFrequency = function (freq) {
    this.oscillator.frequency.setValueAtTime(freq, this.context.currentTime);
}

Sounder.prototype.getTime = function () {
    return this.context.currentTime;
}

Sounder.prototype.beginTone = function (atTime, frequency) {
    this.oscillator.frequency.setValueAtTime(frequency || 600, 0);
    this.gainNode.gain.setTargetAtTime(1.0, atTime || this.context.currentTime, this.rampTime);
}

Sounder.prototype.endTone = function (atTime) {
    this.gainNode.gain.setTargetAtTime(0.0, atTime || this.context.currentTime, this.rampTime);
}

Sounder.prototype.playTone = function (start, length, frequency) {
    var end = start + length;
    this.beginTone(start, frequency);
    this.endTone(end);
    return end;
}
var Keyer = function (wpm, effectiveWpm, farnsworth) {
    this.wpm = wpm;
    this.dot = 0.0;
    this.dash = 0.0;
    this.ta = 0.0;
    this.tc = 0.0;
    this.tw = 0.0;
    this.setRate(wpm, effectiveWpm, farnsworth);
}
    Keyer.prototype.setRate = function (wpm, effectiveWpm, farnsworth) {
        this.wpm = wpm;
        this.effectiveWpm = effectiveWpm;
        this.dot = 1.2 / this.wpm;
        this.dash = this.dot * 3.0;
        this.wordBoundry = this.dot * 7.0;

        if (farnsworth && wpm > effectiveWpm) {
            this.ta = this.getTaSeconds(wpm, effectiveWpm);
            this.tc = this.getTcSeconds(this.ta);
            this.tw = this.getTwSeconds(this.ta);
        } else {
            this.ta = 0;
            this.tc = 0;
            this.tw = 0;
        }
    }
    Keyer.prototype.getTaSeconds = function (wpm, effectiveWpm) {
        return (60.0 * wpm - 37.2 * effectiveWpm) / (wpm * effectiveWpm);
    }
    Keyer.prototype.getTcSeconds = function (ta) {
        return (3.0 * ta) / 19.0;
    }
    Keyer.prototype.getTwSeconds = function (ta) {
        return (7.0 * ta) / 19.0;
    }
    Keyer.prototype.getDot = function () {
        return this.dot;
    }
    Keyer.prototype.getDash = function () {
        return this.dash;
    }
    Keyer.prototype.getWordSpace = function () {
        return this.wordBoundry;
    }
    Keyer.prototype.getSequence = function (str) {
        var start = 0;
        return str.split('').map(x => {
            if (x === " ") {
                return new KeyEvent(this.getWordSpace() + this.tw, false);
            }
            return new KeyEvent(x === "." ? this.getDot() : this.getDash(), true);
        });
    }

var KeyEvent = function (length, isOn) {
    this.length = length;
    this.isOn = isOn;
}


var Player =

    function (wpm, effectiveWpm, farnsworth) {
        this.keyer = new Keyer(wpm, effectiveWpm, farnsworth);
        this.encoder = new Encoder();
        this.sounder = new Sounder();
        this.queue = [];
        this.nextTime = 0;
        this.lookahead = .05;
        this.running = false;
        this.interval = null;
        this.freq = 600;
    }

Player.prototype.getTime = function () {
    return this.sounder.getTime();
}
Player.prototype.setWpm = function (wpm, effectiveWpm, farnsworth) {
    this.keyer = new Keyer(wpm, effectiveWpm, farnsworth);
}
Player.prototype.setFrequency = function (freq) {
    this.freq = freq;
}
Player.prototype.stop = function () {
    this.running = false;
    clearInterval(this.interval);
}
Player.prototype.run = function () {
    var currentTime = this.sounder.getTime();
    while (this.queue.length && (!this.nextTime || this.nextTime <= currentTime + this.lookahead)) {
        var times = this.playNow(this.queue[0], this.nextTime);
        this.nextTime = times[1]
        if (this.onPlayed) {
            this.onPlayed(this.queue[0], times[1]);
        }
        this.queue.splice(0, 1);
    }
    if (!this.queue.length && this.sounder.getTime() > this.nextTime) {
        this.stop();
        if (this.onDone) {
            this.onDone();
        }
    }
}
Player.prototype.start = function (onPlayed, onDone) {
    this.sounder.setFrequency(600);
    this.running = true;
    this.onPlayed = onPlayed;
    this.onDone = onDone;
    this.nextTime = this.sounder.getTime();
    this.run();
    this.interval = setInterval(this.run.bind(this), 10);
}
Player.prototype.queueSentence = function (str) {
    let self = this;
    str.split(' ').forEach(function (x) {
        self.queueWord(x);
    })
}
Player.prototype.queueWord = function (str, frequency) {
    frequency = frequency || this.freq;
    str.split(' ').forEach(x => {
        x.split('').forEach(y => {
            this.queue.push({
                word: y,
                frequency: frequency
            });
        });
        this.queue.push({
            word: ' ',
            frequency: frequency
        });
    });
}
Player.prototype.queueLetter = function (str, frequency) {
    this.queue.push({
        word: str,
        frequency: frequency
    });
}
Player.prototype.playNow = function (item, time) {
    var encodedWord = this.encoder.encode(item.word);
    var sequence = this.keyer.getSequence(encodedWord);
    var next = time;
    if (!sequence || !sequence.length) {
        return next;
    }

    for (var i = 0; i < sequence.length; i++) {
        if (sequence[i].isOn) {
            next = this.sounder.playTone(next, sequence[i].length, item.frequency);
        }
        var doneAt = next;
        next += this.keyer.getDot();
    }
    return [doneAt, next += this.keyer.getDot() * 2 + this.keyer.tc];
}